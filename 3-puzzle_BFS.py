# -*- coding: utf-8 -*-

import numpy as np
import copy


def action(state):
    x,y = np.where(state == 0)
    x = x[0]
    y = y[0]
    result = []
    if x + 1 <= 1:
        state_copy = copy.deepcopy(state)
        temp = state_copy[x][y]
        state_copy[x][y] = state_copy[x+1][y]
        state_copy[x+1][y] = temp
        result.append(state_copy)
    if x - 1 >= 0:
        state_copy = copy.deepcopy(state)
        temp = state_copy[x][y]
        state_copy[x][y] = state_copy[x-1][y]
        state_copy[x-1][y] = temp
        result.append(state_copy)
    if y + 1 <= 1:
        state_copy = copy.deepcopy(state)
        temp = state_copy[x][y]
        state_copy[x][y] = state_copy[x][y+1]
        state_copy[x][y+1] = temp
        result.append(state_copy)
    if y - 1 >= 0:
        state_copy = copy.deepcopy(state)
        temp = state_copy[x][y]
        state_copy[x][y] = state_copy[x][y-1]
        state_copy[x][y-1] = temp
        result.append(state_copy)
        
    return result

def BFS(initialState, goal):
    flag = False
    state = initialState
    path = [initialState]
    costFromRoot = 0
    while not flag:
        if (state == goal).all():
            flag = True
        else:
            possible_moves = action(state)
            j = 0
            for move in possible_moves:
                #checks that move was not made before, if it has then it is deleted from the possible moves
                if any((move == x).all() for x in path):
                    del possible_moves[j]
                j = j + 1
            costFromRoot = costFromRoot + 1
            costs = []
            lowest = 100
            for move in possible_moves:
                cost = costFromRoot
                costToGoal = 0
                #check if possible move is the goal
                if (move == goal).all():
                    path.append(move)
                    flag = True
                    break
                else:
                    #find "the cost to goal" by finding numbers that are in incorrect positions
                    if move[0][0] != goal[0][0]:
                        costToGoal = costToGoal + 1
                    if move[0][1] != goal[0][1]:
                        costToGoal = costToGoal + 1
                    if move[1][0] != goal[1][0]:
                        costToGoal = costToGoal + 1
                    if move[1][1] != goal[1][1]:
                        costToGoal = costToGoal + 1
                    cost = cost + costToGoal
                    costs.append(cost)
            if not flag:
                i = 0
                lowState = -1
                #compares the costs of the possible moves
                #cost is (cost from the root) + (cost to the goal)
                for cost in costs:
                    if cost < lowest:
                        lowest = cost
                        lowState = i
                    i = i + 1
                state = possible_moves[lowState]
                path.append(state)

    return path

if __name__ == '__main__':
    GOAL = np.asarray([[0, 1], [2, 3]])

    initialStates = []
    initialStates.append(np.asarray([[0, 1],[2, 3]]))
    initialStates.append(np.asarray([[2, 1],[0, 3]]))
    initialStates.append(np.asarray([[1, 0],[2, 3]]))
    initialStates.append(np.asarray([[2, 1],[3, 0]]))
    initialStates.append(np.asarray([[1, 3],[2, 0]]))
    initialStates.append(np.asarray([[2, 0],[3, 1]]))
    initialStates.append(np.asarray([[1, 3],[0, 2]]))
    initialStates.append(np.asarray([[0, 2],[3, 1]]))
    initialStates.append(np.asarray([[0, 3],[1, 2]]))
    initialStates.append(np.asarray([[3, 2],[0, 1]]))
    initialStates.append(np.asarray([[3, 0],[1, 2]]))
    initialStates.append(np.asarray([[3, 2],[1, 0]]))

    print "The goal state is: "
    print GOAL
    print "--------------------------------"
    for state in initialStates:
        results = BFS(state, GOAL)

        print "For the initial state: "
        print state
        print "Path from start to goal: "
        print results
        print "--------------------------------"
